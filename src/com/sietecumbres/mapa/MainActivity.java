package com.sietecumbres.mapa;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public abstract class MainActivity extends android.support.v4.app.FragmentActivity 
{
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		// Esto lo har� cada Activity con su vista:
		// setContentView(R.layout.activity_default);
	}


	protected void onDestroy ()
	{
		super.onDestroy ();
	}

	protected void onPause ()
	{
		super.onPause ();
	}

	protected void onRestart ()
	{
		super.onRestart ();
	}

	protected void onResume ()
	{
		super.onResume ();
	}

	protected void onStart ()
	{
		super.onStart ();
	}

	protected void onStop ()
	{
		super.onStop ();
	}

	// Click Methods HERE:


	public void onClickFeature (View v)
	{
		int id = v.getId ();
		switch (id) {
		case R.id.ep_map_button:
			lanzarMapa();

			break;
		case R.id.ep_gps_button :   
			lanzarLocationSettings();

		default: 
			break;
		}
	}

	private void lanzarMapa() {
		Intent i = new Intent(this, MapActivity.class);
		i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
		startActivity(i);
	}  

	private void lanzarLocationSettings() {
		Intent i = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS); 
		startActivity(i);
	} 

	public void lanzarEntryPoint(Context context) 
	{
		final Intent intent = new Intent(context, EntryPointActivity.class);
		intent.setFlags (Intent.FLAG_ACTIVITY_CLEAR_TOP);
		context.startActivity (intent);
	}

	public void setTitleFromActivityLabel (int textViewId)
	{
		TextView tv = (TextView) findViewById (textViewId);
		if (tv != null) tv.setText (getTitle ());
	} 


	public void toast (String msg)
	{
		Toast.makeText (getApplicationContext(), msg, Toast.LENGTH_SHORT).show ();
	}

	public void trace (String msg) 
	{
		Log.d("La Meca :: ", msg);
		toast (msg);
	}

} 