package com.sietecumbres.mapa;

import android.location.LocationManager;
import android.os.Bundle;
import android.widget.Button;

public class EntryPointActivity extends MainActivity {
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	@Override
	protected void onResume() {
		setMapButtonVisibility();
		super.onResume();
	}

	private void setMapButtonVisibility() {
		LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

		boolean network_enabled=locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
		boolean gps_enabled=locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
		Button mapButton = ((Button)this.findViewById(R.id.ep_map_button)); 
		if (!(network_enabled || gps_enabled)){

			mapButton.setEnabled(false);
		}
		else {
			mapButton.setEnabled(true);
		}
	}
}