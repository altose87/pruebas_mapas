package com.sietecumbres.mapa;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.LocationSource;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

public class MapActivity extends MainActivity implements LocationListener, LocationSource, SensorEventListener {
	// El mapa
	private GoogleMap mMap;
	// Para escuchar los cambios de localizaci�n del usuario, 
	// si se mueve o si la localizaci�n es m�s precisa
	private OnLocationChangedListener mListener;
	// Para gestionar nosotros la localizaci�n y no el propio mapa y poder 
	// manejar eventos como onLocationChanged que de otra manera no podr�amos gestionar
	private LocationManager locationManager;

	private boolean haveLocation = false;

	// Sensores para orientar el mapa de acuerdo a la br�jula
	private SensorManager mSensorManager;
	private float[] mGravity;
	private float[] mGeomagnetic;

	// para controlar el intervalo de ejecuci�n del evento de los sensores
	private long lastTimeCompassUpdated = 0;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_map);       

		// Instanciamos locationManager
		locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
		// Establecemos la precisi�n
		Criteria locationCriteria = new Criteria();
		locationCriteria.setAccuracy(Criteria.ACCURACY_FINE);
		// Y establecemos que se escuche al mejor proveedor de localizaci�n 
		// (o redes m�viles o GPS o WiFi, depender� del estado del dispositivo
		// y as� nos despreocupamos nosotros)
		locationManager.requestLocationUpdates(locationManager.getBestProvider(locationCriteria, true), 1L, 2F, (LocationListener) this);

		initializeMap();       
	}

	private void initializeMap() {
		// Confirmamos que no se ha inicializado el mapa todav�a
		if (mMap == null) 
		{
			// obtenemos el mapa de la vista
			mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();

			// Registramos o establecemos �sta clase, MapActivity, como LocationSource 
			// del mapa para utilizar nuestro locationManager y el Listener ;)
			mMap.setLocationSource(this);

			// Inicializamos el mapa...
			if (mMap != null) 
			{
				setUpMap();     
			}
		}
	}

	private void setUpMap() 
	{
		// Nos localiza...
		mMap.setMyLocationEnabled(true);
		// Quitamos el bot�n de "mi posici�n"
		mMap.getUiSettings().setMyLocationButtonEnabled(false);
		// Pinta una marca en La Meca
		addLaMecaMarkOnMap();
		centerMapOnLaMeca();
	}

	private void addLaMecaMarkOnMap(){
		mMap.addMarker(new MarkerOptions()
		.position(new LatLng(21.4225222672, 39.8261238221))
		.title("La Meca")
		.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_launcher)));
	}

	public void centerMapOnLaMeca(){
		CameraPosition newCameraPosition = new CameraPosition.Builder()
		.target(new LatLng(21.4225222672, 39.8261238221))      
		.zoom(1)                   
		.bearing(0)        
		.tilt(0)                  
		.build();  

		mMap.animateCamera(CameraUpdateFactory.newCameraPosition(newCameraPosition));
	}

	private void initialiceSensors() {
		mSensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
		// Vamos a usar el aceler�metro y el sensor magn�tico
		Sensor gsensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		Sensor msensor = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);

		mSensorManager.registerListener(this, gsensor, SensorManager.SENSOR_DELAY_NORMAL); 
		mSensorManager.registerListener(this, msensor, SensorManager.SENSOR_DELAY_NORMAL);
	} 

	private void drawLineBetweenOurLocationAndLaMeca(){
		// Debe ejecutarse cuando sepamos nuestra localizaci�n
		// es decir, onLocationChanged
		Location myLocation = mMap.getMyLocation();
		LatLng myLatLng = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
		LatLng laMecaLatLng = new LatLng(21.4225222672, 39.8261238221);

		// creamos un polyline (linea poligonal) con dos puntos
		PolylineOptions rectOptions = new PolylineOptions()
		.add(myLatLng) // nuestra posici�n
		.add(laMecaLatLng); // la posici�n de la meca

		// pintamos esa l�nea de color verde
		rectOptions.color(Color.GREEN);

		// Y la a�adimos al mapa
		mMap.addPolyline(rectOptions); 
	}

	@Override
	public void onPause()
	{
		if(locationManager != null)
		{
			locationManager.removeUpdates((LocationListener) this);
		}

		super.onPause();
	}

	@Override
	public void onResume()
	{
		super.onResume();

		initializeMap();

		if(locationManager != null)
		{
			mMap.setMyLocationEnabled(true);
		}
	}


	public void activate(OnLocationChangedListener listener) 
	{
		mListener = listener;
	}

	public void deactivate() 
	{
		mListener = null;
	}

	public void onLocationChanged(Location location) 
	{
		if (!haveLocation){
			initialiceSensors();
			haveLocation = true;
		}
		mMap.clear();
		if( mListener != null )
		{
			addLaMecaMarkOnMap();

			mListener.onLocationChanged( location );
			CameraPosition newCameraPosition = new CameraPosition.Builder()
			.target(new LatLng(location.getLatitude(), location.getLongitude()))      // centra el mapa en nuestra posici�n
			.zoom(19)   // establece el zoom
			.bearing(0)        
			.tilt(0)                  
			.build();  

			//Move the camera to the user's location once it's available!
			mMap.animateCamera(CameraUpdateFactory.newCameraPosition(newCameraPosition)); 
			drawLineBetweenOurLocationAndLaMeca();

		}
	}

	public void onProviderDisabled(String provider) 
	{
		// cuando se deshabilita un probeedor de localizaci�n... prefiero no hacer nada
		// Toast.makeText(this, "provider disabled", Toast.LENGTH_SHORT).show();
	}

	public void onProviderEnabled(String provider) 
	{
		// cuando se habilita un probeedor de localizaci�n... prefiero no hacer nada
		// Toast.makeText(this, "provider enabled", Toast.LENGTH_SHORT).show();
	}

	public void onStatusChanged(String provider, int status, Bundle extras) 
	{
		// Cuando cambia el estado de la localizaci�n... prefiero no hacer nada
		// Toast.makeText(this, "status changed", Toast.LENGTH_SHORT).show();
	}

	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// cuando cambia la precisi�n...
	}


	public void onSensorChanged(SensorEvent event) {
		// quiero dejar un margen de 3 segundos al principio
		// para la animaci�n hasta el punto donde se encuentra el usuario...
		if (lastTimeCompassUpdated == 0){
			lastTimeCompassUpdated = System.currentTimeMillis()+3000;
			return;
		}
		else if (System.currentTimeMillis()>lastTimeCompassUpdated ){
			if (mMap!=null){
				int matrix_size = 16;
				float[] R = new float[matrix_size];
				float[] outR = new float[matrix_size];

				if (event.accuracy == SensorManager.SENSOR_STATUS_UNRELIABLE)
					return;

				switch (event.sensor.getType()) {
				case Sensor.TYPE_MAGNETIC_FIELD:
					mGeomagnetic = event.values.clone();
					break;
				case Sensor.TYPE_ACCELEROMETER:
					mGravity = event.values.clone();
					break;
				}

				if (mGeomagnetic != null && mGravity != null) {
					if (SensorManager.getRotationMatrix(R, null, mGravity, mGeomagnetic)) {
						SensorManager.getOrientation(R, outR);
					}

					CameraPosition oldPosition = mMap.getCameraPosition();

					// Quiero que s�lo se actualice si hay una variaci�n de mas de un grado
					//if (Math.abs(Math.abs(oldPosition.bearing) - Math.abs(Math.toDegrees(outR[0])))>1){

					CameraPosition newCameraPosition = new CameraPosition.Builder()
					.target(oldPosition.target)      // Sets the center of the map to Mountain View
					.zoom(oldPosition.zoom)                   // Sets the zoom
					.bearing((float) Math.toDegrees(outR[0]))        
					.tilt(oldPosition.tilt)                 
					.build();                   // Creates a CameraPosition from the builder

					mMap.moveCamera(CameraUpdateFactory.newCameraPosition(newCameraPosition));

				}
			}
		}
	}
}